﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc_5_Empty_Template1.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string NewsMessage { get; set; }           
        public string NewsTitle { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Category Category { get; set; }
        //public int CategoryId { get; set; }
    }
}