﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc_5_Empty_Template1.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string NameCategory { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
    }
}