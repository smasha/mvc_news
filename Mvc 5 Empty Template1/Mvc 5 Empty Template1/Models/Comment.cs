﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc_5_Empty_Template1.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Message { get; set; }
        public Article Article { get; set; }
    }
}