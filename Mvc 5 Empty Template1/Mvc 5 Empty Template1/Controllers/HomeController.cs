﻿using System.Web.Mvc;
using System.Collections.Generic;
using Mvc_5_Empty_Template1.Models;

namespace Mvc_5_Empty_Template1.Controllers
{
    public class HomeController : Controller
    {       
        NewsSiteContext db = new NewsSiteContext();

        public ActionResult Index()
        {
            int n = 3;
            List<Article> top = new List<Article>();
            foreach (var article in db.Articles)
            {
                if (n < 1) break;
                top.Add(article);
                n--;
            }
            return View("News", top);
        }

        public ActionResult Read(int id)
        {
            ViewBag.Id = id;
            var article = new Article();
            article = db.Articles.Find(ViewBag.Id);
            return View(article);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.AvailableCategories = db.Categories;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Article article)
        //public ActionResult Create([Bind(Include = "NewsTitle,NewsMessage,Category")] Article article)
        {
            ViewBag.AvailableCategories = db.Categories;
            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Something is invalid";
                return View(article);
            }
            var cat = db.Categories.Find(article.Category.Id);
            if (cat == null)
            {
                ViewBag.ErrorMessage = "Category is invalid";
                return View(article);
            }
            article.Category = cat;
            db.Articles.Add(article);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CreateComment(int aid, Comment comment)        
        {
            var article = db.Articles.Find(aid);
            if (article != null)
            {
                comment.Article = article;
                db.Comments.Add(comment);
                db.SaveChanges();
            }
            return RedirectToAction("Read/" + aid);
        }


        public ActionResult News()
        {           
            return View(db.Articles);
        }
    }
}